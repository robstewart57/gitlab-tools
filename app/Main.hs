{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import GitLab.Tools.AddGroupToForks
import GitLab.Tools.AddGroupToProjects
import GitLab.Tools.AddReportersToGroup
import GitLab.Tools.AddUserToProjects
import GitLab.Tools.DeleteProjects
import GitLab.Tools.DownloadProjects
import GitLab.Tools.IsUserRegistered
import GitLab.Tools.MergeRequest
import GitLab.Tools.Plagiarism
import GitLab.Tools.ProjectDetails
import GitLab.Tools.ProjectForks
import GitLab.Tools.RemoveMembersFromGroup
import GitLab.Tools.Types
import Options.Applicative
import System.Environment
import System.IO

main :: IO ()
main = processOptions =<< execParser opts
  where
    opts =
      info
        (parser <**> helper)
        ( fullDesc
            <> progDesc "Executes actions against a GitLab server"
            <> header "Program to execute bulk GitLab actions"
        )

parser :: Parser Options
parser =
  Options
    <$> strOption
      ( long "host"
          <> metavar "host"
          <> help "URL of the GitLab server"
      )
    <*> strOption
      ( long "usersFilename"
          <> metavar "usersFilename"
          <> value ""
          <> help "use only the users of this named GitLab group, this flag is the name of a CSV file with user IDs with all comma separated usernames on one line in the file"
      )
    <*> strOption
      ( long "usersGroup"
          <> metavar "usersGroup"
          <> value ""
          <> help "use only the users of this named GitLab group"
      )
    <*> strOption
      ( long "user"
          <> metavar "user"
          <> value ""
          <> help "username of a user"
      )
    <*> strOption
      ( long "group"
          <> metavar "group"
          <> value ""
          <> help "name of a group"
      )
    <*> strOption
      ( long "group-id"
          <> metavar "group-id"
          <> value ""
          <> help "ID of a group"
      )
    <*> strOption
      ( long "project"
          <> metavar "project"
          <> value ""
          <> help "name of a project"
      )
    <*> strOption
      ( long "project-id"
          <> metavar "project-id"
          <> value ""
          <> help "ID of a project"
      )
    <*> switch
      ( long "add-reporters-to-group"
          <> help "Add users as reporters to a group"
      )
    <*> switch
      ( long "add-group-to-projects"
          <> help "Add group as a Reporter to all projects with a given name with --project"
      )
    <*> switch
      ( long "add-group-to-forks"
          <> help "Add group as a Reporter to all forks of project with a given ID with --project-id"
      )
    <*> switch
      ( long "add-user-to-project-forks"
          <> help "Add user as a Reporter to all project forks with a given name with --project or ID with --project-id"
      )
    <*> switch
      ( long "add-user-to-projects-in-group"
          <> help "Add a user as a Reporter to forks of all projects in a given group stated with --group or --group-id"
      )
    <*> switch
      ( long "remove-members-from-group"
          <> help "Remove all members from a group"
      )
    <*> switch
      ( long "registered"
          <> help "prints 'yes' or 'no' depending on whether a user exists on the GitLab server"
      )
    <*> switch
      ( long "plagiarism"
          <> help "use MOSS to detect plagiarism"
      )
    <*> strOption
      ( long "mossUser"
          <> metavar "mossUser"
          <> value ""
          <> help "MOSS user ID"
      )
    <*> strOption
      ( long "mossLang"
          <> metavar "mossLang"
          <> value ""
          <> help "languge to pass to MOSS"
      )
    <*> strOption
      ( long "mossMaxMatches"
          <> metavar "mossMaxMatches"
          <> value "250"
          <> help "the maximum number of matches to MOSS to perform"
      )
    <*> strOption
      ( long "plagiarismProject"
          <> metavar "plagiarismProject"
          <> value ""
          <> help "project to detect plagiarism in"
      )
    <*> strOption
      ( long "plagiarismFiles"
          <> metavar "plagiarismFiles"
          <> value ""
          <> help "comma separated list of plagiarism files"
      )
    <*> strOption
      ( long "parent"
          <> metavar "parent"
          <> value ""
          <> help "namespace of username/group/subgroup of template code for MOSS (provide full namespace, not just subgroup name)"
      )
    <*> switch
      ( long "projectDetails"
          <> help "gets project info for searched projects with a given name"
      )
    <*> switch
      ( long "forks"
          <> help "gets project info for forks of a given project"
      )
    <*> switch
      ( long "mergeRequest"
          <> help "open merge requests"
      )
    <*> strOption
      ( long "mergeRequestSourceNamespace"
          <> metavar "mergeRequestSourceNamespace"
          <> value ""
          <> help "namespace of the source project of merge request"
      )
    <*> strOption
      ( long "mergeRequestTitle"
          <> metavar "mergeRequestTitle"
          <> value ""
          <> help "merge request title"
      )
    <*> strOption
      ( long "mergeRequestDescription"
          <> metavar "mergeRequestDescription"
          <> value ""
          <> help "merge request description"
      )
    <*> strOption
      ( long "mergeRequestSourceBranch"
          <> metavar "mergeRequestSourceBranch"
          <> value ""
          <> help "branch name for source project of merge request"
      )
    <*> strOption
      ( long "mergeRequestTargetBranch"
          <> metavar "mergeRequestTargetBranch"
          <> value ""
          <> help "branch name for target project of merge request"
      )
    <*> switch
      ( long "downloadProjects"
          <> help "download project zip files"
      )
    <*> switch
      ( long "deleteProjects"
          <> help "delete projects with a name stated with --project or --project-id"
      )
    <*> strOption
      ( long "branch"
          <> metavar "branch"
          <> value ""
          <> help "branch name for MOSS detection (usually \"main\" or \"master\")"
      )

processOptions :: Options -> IO ()
processOptions opts = do
  hSetBuffering stdout NoBuffering
  gTokenSearch <- lookupEnv "GITLAB_TOKEN"
  gitLabToken <- case gTokenSearch of
    Nothing -> enterToken
    Just "" -> enterToken
    Just tok -> useEnvironVar tok
  go gitLabToken
  where
    useEnvironVar tok = do
      putStr "GITLAB_TOKEN environment variable found, use this? (y/n) "
      resp <- getLine
      case resp of
        "y" -> return tok
        "n" -> enterToken
        _ -> putStrLn "Pleae answer y or n" >> useEnvironVar tok
    enterToken = do
      putStr "Enter your GitLab token: "
      getLine
    go tok
      | addReportersToGroup opts =
          addReportersToGroupGo opts tok
      | addGroupToProjects opts =
          addGroupToProjectsGo opts tok
      | addGroupToForks opts =
          addGroupToForksGo opts tok
      | addUserToProjectForks opts =
          addUserToProjectForksGo opts tok
      | addUserToProjectsInGroup opts =
          addUserToProjectsInGroupGo opts tok
      | removeMembersFromGroup opts =
          removeMembersFromGroupGo opts tok
      | isRegistered opts =
          isUserRegisteredGo opts tok
      | plagiarism opts =
          plagiarismGo opts tok
      | projectDetails opts =
          projectDetailsGo opts tok
      | forks opts =
          projectForksGo opts tok
      | mergeRequestOpt opts =
          mergeRequestGo opts tok
      | deleteProjects opts =
          deleteProjectsGo opts tok
      | downloadProjects opts =
          downloadProjectsGo opts tok
      | otherwise = error "combination of flags not recognised"
