# Command line tool: gitlab-tools

This is a command line tool for bulk transactions against a GitLab
server.

## Features

1. Add a list of reporters to a group.
2. Add a group as a member to all projects with a given name.
3. Print details of projects with a given name.
4. Submit specific files to MOSS for plagiarism detection.

## Installation

### Install stack
    
If you have `stack` installed, skip this bit.
    
__Note:__ with the instruction below about installing `stack`, only
use the https://get.haskellstack.org/ script if you have `sudo` or
root access to your machine. If not, read further down the stack
installation page for installing `stack` in userland.

Install the `stack` tool using these
[instructions](https://docs.haskellstack.org/en/stable/install_and_upgrade/).

### Install gitlab-tools

Clone this repository:

    git clone https://gitlab.com/robstewart57/gitlab-tools.git
    
Then compile and install it:

    cd gitlab-tools
    stack install
    
Make sure that `stack` installs the `gitlab-tools` executable file
into a directory which is included in your `$PATH`. On Linux, stack
installs to `~/.local/bin/`, so make sure that this directory is part
of `$PATH`.

## Usage

### Create an access token in GitLab

Go to your profile on the GitLab server you are using, e.g.

    https://<gitlab.example.com>/profile

Click _Access Tokens_ on the left.

Click _Create personal access token_.

Keep a record of the token, it is needed by `gitlab-tools` which will
ask you for this token when you run it.

This token will grant you access to any GitLab data that you are able
to see on the GitLab web interface. Think of it as just another way to
viewing GitLab data, with the same access level if browsing the GitLab
web interface.

### Using gitlab-tools

```
$ gitlab-tools --help
Program to execute bulk GitLab actions

Usage: gitlab-tools --host host [--usersFilename usersFilename] 
                    [--usersGroup usersGroup] [--user user] [--group group] 
                    [--project project] [--project-id project-id] 
                    [--add-reporters-to-group] [--add-group-to-projects] 
                    [--add-user-to-projects] [--add-user-to-projects-in-group] 
                    [--remove-members-from-group] [--registered] [--plagiarism] 
                    [--mossUser mossUser] [--mossLang mossLang] 
                    [--plagiarismProject plagiarismProject] 
                    [--plagiarismFiles plagiarismFiles] [--parent parent] 
                    [--projectDetails] [--forks] [--mergeRequest] 
                    [--mergeRequestSourceNamespace mergeRequestSourceNamespace] 
                    [--mergeRequestTitle mergeRequestTitle] 
                    [--mergeRequestDescription mergeRequestDescription] 
                    [--mergeRequestSourceBranch mergeRequestSourceBranch] 
                    [--mergeRequestTargetBranch mergeRequestTargetBranch] 
                    [--downloadProjects] [--branch branch]
  Executes actions against a GitLab server

Available options:
  --host host              URL of the GitLab server
  --usersFilename usersFilename
                           use only the users of this named GitLab group, this
                           flag is the name of a CSV file with user IDs with all
                           comma separated usernames on one line in the file
  --usersGroup usersGroup  use only the users of this named GitLab group
  --user user              username of a user
  --group group            name of a group
  --project project        name of a project
  --project-id project-id  ID of a project
  --add-reporters-to-group Add users as reporters to a group
  --add-group-to-projects  Add group as a Reporter to all projects with a given
                           name with --project
  --add-user-to-projects   Add user as a Reporter to all projects with a given
                           name with --project
  --add-user-to-projects-in-group
                           Add a user as a Reporter to forks of all projects in
                           a given group stated with --group
  --remove-members-from-group
                           Remove all members from a group
  --registered             prints 'yes' or 'no' depending on whether a user
                           exists on the GitLab server
  --plagiarism             use MOSS to detect plagiarism
  --mossUser mossUser      MOSS user ID
  --mossLang mossLang      languge to pass to MOSS
  --plagiarismProject plagiarismProject
                           project to detect plagiarism in
  --plagiarismFiles plagiarismFiles
                           comma separated list of plagiarism files
  --parent parent          namespace of username/group/subgroup of template code
                           for MOSS (provide full namespace, not just subgroup
                           name)
  --projectDetails         gets project info for searched projects with a given
                           name
  --forks                  gets project info for forks of a given project
  --mergeRequest           open merge requests
  --mergeRequestSourceNamespace mergeRequestSourceNamespace
                           namespace of the source project of merge request
  --mergeRequestTitle mergeRequestTitle
                           merge request title
  --mergeRequestDescription mergeRequestDescription
                           merge request description
  --mergeRequestSourceBranch mergeRequestSourceBranch
                           branch name for source project of merge request
  --mergeRequestTargetBranch mergeRequestTargetBranch
                           branch name for target project of merge request
  --downloadProjects       download project zip files
  --branch branch          branch name for MOSS detection (usually "main" or
                           "master")
  -h,--help                Show this help text
```

### Avoid typing your GitLab token

By default `gitlab-tools` will ask you to enter your GitLab token:

    Enter your GitLab token:

If you wish to avoid typing your GitLab token each time, you can instead store
your token in an environment variable, e.g. in `~/.profile`:

    export GITLAB_TOKEN=abc1234567890

`gitlab-tools` will find this and offer you the choice about whether to use
token, or type in a different one:

    GITLAB_TOKEN environment variable found, use this? (y/n) 

If you type `y` then hit enter, it will use the token found for the
`GITLAB_TOKEN` environment variable. If you type `n`, it will prompt you to
enter a token.

## Examples

### Adding users to a group

```
gitlab-tools \
    --host <GitLab url> \
    --filename <filename>.csv \
    --add-reporters-to-group \
    --group <group name>
```

This command reads usernames from a comma separated file, and if they
are registered on the GitLab server it adds them to the `<group name>`
group with the _Reporter_ role.

### Removing all users from a group

```
gitlab-tools \
    --host <GitLab url>
    --remove-members-from-group
    --group <group name>
```

This command removes all members from the group `<group name>`. Alternatively
`--group-id` can be used with a group with the given ID.

### Getting details of projects

```
gitlab-tools \
    --host <GitLab url> \
    --projectDetails \
    --project <project name>
```

This finds all projects named `project_name`. For each project found,
it will print:

* Continuous Integration result for the most recent commit.
* How many members the project has.
* The visibility of the project (private, internal or public).
* How many commits the project has.

### Getting details of forks of a project

```
gitlab-tools \
    --host <GitLab url> \
    --forks \
    --project-id <project ID>
```

This finds all forks of the project with ID `project ID`. For each fork,
it will print:

* The name and username of the project owner.
* The URL of the project.
* Continuous Integration result for the most recent commit.
* The timestamp and hash of the most recent commit.
* The visibility of the project (private, internal or public).
* How many commits the project has.

If you wish to restrict the forks reported on to a subset of users who may have
forked the project, you can pass the `--usersFilename` flag containing a comma
separated list of usernames:

```
gitlab-tools \
    --host <GitLab url> \
    --forks \
    --project-id <project ID>
    --usersFilename <file path>
```


### Getting details of projects for specific users

```
gitlab-tools \
    --host <GitLab url> \
    --projectDetails \
    --project <project name> \
    --usersFilename <CSV file path>
```

This attempts to find, for all user IDs in the CSV file, a project
named `project_name`. If the user does not have a project with that
name, this will be reported. When the user _does_ have a project with
name `project_name`, it will print:

* Continuous Integration result for the most recent commit.
* How many members the project has.
* The visibility of the project (private, internal or public).
* How many commits the project has.

### Adding a user to projects

```
gitlab-tools \
    --host <GitLab url> \
    --add-user-to-projects --project <project name> \
    --user <username>
```

This commands finds all projects called `<project name>` then adds the
`<username>` user to all of those projects as a member with the
Reporter role.

### Adding a group to projects

```
gitlab-tools \
    --host <GitLab url> \
    --add-group-to-projects --project <project name> \
    --group <group name>
```

This commands finds all projects called `<project name>` then adds the
`<group name>` group to all of those projects as a member with the
Reporter role.

### Adding a group to forks of a project

You can add a group as a Reporter member to forks of a project with a given
project ID using `--add-group-to-forks`:

```
gitlab-tools \
    --host <GitLab url> \
    --add-group-to-forks --project-id <project id> \
    --group namespace1/group1
```

This will add the group `group1` in namespace `namespace1` as a member of all
forks of a project with project ID `<project id>`.

You can also filter the forks that the group will be added to, by either using
`--usersGroup` or `--usersFilename`. E.g.

```
gitlab-tools \
    --host <GitLab url> \
    --add-group-to-forks --project-id <project id> \
    --group namespace1/group1 \
    --usersGroup namespace2/group2
```

This command above will find the usernames of members of the group
`namespace2/group2`, and filter the forks: if the owner of the fork is in that
list of found usernames then the forked project is included, otherwise it is
not.

### Creating merge requests

```
gitlab-tools \
    --host <GitLab url> \
    --mergeRequest
    --usersFilename <CSV file path> \
    --mergeRequestSourceNamespace <source namespace> \
    --mergeRequestTitle <title> \
    --mergeRequestDescription <description> \
    --project <project>
```

This command finds all projects called `<project name>` in the
namespace of all users in the `<CSV file path>` file, and creates a
merge request for each one. The source project for the merge requests
has the same project name, and the namespace `<source namespace>`. The
title of the merge request will be `<title>` e.g. "This is a
title". The free text description in the merge request will be
`<description>` e.g. "This is a description". Optionally, you can
specify the branch names of the source and target projects in the
merge request with flags `--mergeRequestSourceBranch` and
`--mergeRequestTargetBranch`. These branch names default to "master"
if not specified.

### Download projects as archive files

```
gitlab-tools \
    --host <GitLab url> \
    --downloadProjects \
    --project <project> \
    --usersFilename <CSV file path>
```

This command finds all projects called `<project name>` in the namespace of all
users in the `<CSV file path>` file, then downloads the projects. Each project
is downloaded as a zip file, one zip file per user. Alternatively the
`--project-id` flag can be used with a project ID, where downloaded projects
will be the forks of the project with that ID. Moreover the `--usersGroup` flag
can be used to filter downloaded projects by the members of the specified group
name.

### Delete projects

```
gitlab-tools \
    --host <GitLab url> \
    --deleteProjects \
    --project <project>
```

This command deletes all projects called `<project name>`. Alternatively, the
`--project-id <project ID>` flag can be used in place on `--project`, which will
delete a single project with the given project ID.

### Check for code plagiarism

This is useful in the context assessing code in an education setting.

```
gitlab-tools \
  --host <GitLab url> \
  --plagiarism \
  --mossUser <your MOSS user key> \
  --plagiarismFiles <files> \
  --parent <starter code user> \
  --usersFilename <CSV user file path> \
  --plagiarismProject <project name> \
  --mossLang <programming language>
```
  
Where:

* `<your MOSS user key>` is generated by MOSS, see
  [here](http://theory.stanford.edu/~aiken/moss/)

* `<files>` is a string with comma separated file paths in the
  repository. These files are the ones that MOSS will check, i.e. the
  ones you think is at risk of plagiarism. E.g "src/Foo.hs,src/Bar.hs"

* `<starter code user>` is the user that has the "starter code" in a
  project in their namespace, from which other people have forked to
  work on their own versions.

* `<CSV file path>` is a full path for a CSV file containing comma
  separated user IDs of users to be checked for plagiarism.
  
* `<project name>` is the name of the project that each user will have
  forked to complete their work.
  
* `<programming language>` is to help MOSS detect plagiarism, by
  telling MOSS which programming language it is checking plagiarism
  for. Support languages are: C, CPP, Java, CSharp, Python,
  VisualBasic, Javascript, FORTRAN, ML, Haskell, Lisp, Scheme, Pascal,
  Modula2, Ada, Perl, TCL, Matlab, VHDL and Verilog. 

GitLab user IDs are anonymised before code is sent to
MOSS. `gitlab-tools` prints the mapping from anonymised IDs to real
GitLab IDs.

# Adoption of gitlab-tools

This tool was initially developed to automate GitLab for use in computer science
education. See our ICSE-SEET 2024 paper for the details: [_"Integrating Canvas
and GitLab to Enrich Learning
Processes"_](https://doi.org/10.1145/3639474.3640056).
