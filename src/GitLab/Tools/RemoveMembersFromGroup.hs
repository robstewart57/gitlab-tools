{-# LANGUAGE OverloadedStrings #-}

module GitLab.Tools.RemoveMembersFromGroup where

import Control.Monad.IO.Class
import Data.Maybe
import qualified Data.Text as T
import GitLab
import GitLab.Tools.Types

removeMembersFromGroupGo :: Options -> String -> IO ()
removeMembersFromGroupGo opts tok
  | removeMembersFromGroup opts && null (groupOpt opts) && null (groupIdOpt opts) =
      putStrLn "--remove-members-from-group requires --group <group name> or --group-id <group ID>"
  | removeMembersFromGroup opts && not (null (groupOpt opts)) && not (null (groupIdOpt opts)) =
      putStrLn "only one of --group <group name> or --group-id <group ID> can be used with --remove-members-from-group"
  | removeMembersFromGroup opts = do
      runGitLab
        ( defaultGitLabServer
            { url = T.pack (gitlabUrl opts),
              token = AuthMethodToken (T.pack tok)
            }
        )
        ( do
            grps <-
              if not (null (groupOpt opts))
                then searchGroup (T.pack (groupOpt opts))
                else do
                  result <- group (read (groupIdOpt opts))
                  case result of
                    Left err -> error (show err)
                    Right Nothing -> return []
                    Right (Just g) -> return [g]
            case grps of
              [] -> liftIO (putStrLn ("--remove-members-from-group : group " <> groupOpt opts <> " not found"))
              [grp] -> do
                Right foundMembers <- membersOfGroup grp
                mapM_
                  ( \mbr -> do
                      Right (Just usr) <- user (member_id mbr)
                      attempt <- removeUserFromGroup grp usr
                      case attempt of
                        Left err ->
                          liftIO $
                            putStrLn
                              ( "HTTP error removing member from group "
                                  <> show err
                              )
                        Right Nothing ->
                          liftIO $
                            putStrLn
                              ( "Not possible to remove user "
                                  <> show (member_id mbr)
                                  <> " from "
                                  <> if not (null (groupOpt opts))
                                    then groupOpt opts
                                    else groupIdOpt opts
                              )
                        Right (Just ()) ->
                          liftIO $
                            putStrLn
                              ( T.unpack (fromJust (member_username mbr))
                                  <> " removed from group "
                                  <> groupOpt opts
                              )
                  )
                  foundMembers
              (_ : _) -> liftIO (putStrLn ("--remove-members-from-group : too many groups found for " <> groupOpt opts))
            return ()
        )
  | otherwise =
      error "--remove-members-from-group flag combinations not supported."
