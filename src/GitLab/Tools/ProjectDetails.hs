{-# LANGUAGE OverloadedStrings #-}

module GitLab.Tools.ProjectDetails where

import Control.Monad
import Control.Monad.IO.Class
import Data.Char (isSpace)
import Data.List (dropWhileEnd)
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import GitLab
import GitLab.Tools.Types
import System.Directory

projectDetailsGo :: Options -> String -> IO ()
projectDetailsGo opts tok
  | projectDetails opts && null (projectOpt opts) =
      error "--projectDetails must also be used with --project"
  -- --projectDetails specified with --usersFilename, so search for
  -- projects (--project) only forked by users in the .csv file.
  | projectDetails opts && not (null (usersFilename opts)) = do
      putStrLn ("finding details for project: " <> projectOpt opts)
      exists <- doesFileExist (usersFilename opts)
      unless exists $
        error ("file does not exist: " <> usersFilename opts)
      usersData <- readFile (usersFilename opts)
      let usernames = T.splitOn "," (T.pack usersData)
      runGitLab
        ( defaultGitLabServer
            { url = T.pack (gitlabUrl opts),
              token = AuthMethodToken (T.pack tok)
            }
        )
        ( do
            forksCsv <- do
              catMaybes
                <$> mapM
                  ( \userName -> do
                      prjts <-
                        projectWithPathAndName userName (T.pack (projectOpt opts))
                      case prjts of
                        Left _err -> do
                          liftIO (putStrLn (T.unpack userName <> " hasn't forked " <> projectOpt opts))
                          return Nothing
                        Right Nothing -> do
                          liftIO (putStrLn (T.unpack userName <> " hasn't forked " <> projectOpt opts))
                          return Nothing
                        Right (Just proj) -> do
                          liftIO (putStrLn ("Obtaining " <> T.unpack userName <> " fork of " <> projectOpt opts))
                          studentCsvLine <- studentProjectInfo proj
                          return (Just studentCsvLine)
                  )
                  usernames
            liftIO (putStrLn "\nPrinting CSV...\n")
            mapM_ (liftIO . putStrLn . T.unpack) forksCsv
        )
  -- --projectDetails specified but not --usersFilename,
  -- so instead use --project and just search for projects instead.
  | projectDetails opts && null (usersFilename opts) = do
      putStrLn ("finding details for project: " <> projectOpt opts)
      runGitLab
        ( defaultGitLabServer
            { url = T.pack (gitlabUrl opts),
              token = AuthMethodToken (T.pack tok)
            }
        )
        ( do
            prjcts <- projectsWithName (T.pack (projectOpt opts))
            studentCsvLines <- mapM studentProjectInfo prjcts
            liftIO (putStrLn "\nPrinting CSV...\n")
            liftIO (putStrLn (T.unpack csvHeaders))
            mapM_ (liftIO . putStrLn . T.unpack) studentCsvLines
        )
  | otherwise =
      error "--projectDetails flag combinations not supported."

csvHeaders :: Text
csvHeaders =
  ","
    `T.intercalate` [ "name",
                      "member count",
                      "members",
                      "URL",
                      "CI",
                      "last commit date",
                      "last commit hash",
                      "visibility",
                      "number of commits"
                    ]

studentProjectInfo :: Project -> GitLab Text
studentProjectInfo proj = do
  isPassing <- projectCISuccess proj
  commits <- repoCommits proj
  projMembers <- membersOfProject proj
  let details =
        [ case project_owner proj of
            Nothing -> ""
            Just own -> formatName (owner_name own),
          T.pack (show (length (projMembers))),
          T.intercalate ";" (map (fromJust . member_username) projMembers),
          project_web_url proj,
          if isPassing then "passing" else "failing",
          case length commits of
            0 -> ""
            _ -> fromJust (commit_committed_date (head commits)),
          case length commits of
            0 -> ""
            _ -> commit_id (head commits),
          fromMaybe "" (project_visibility proj),
          T.pack (show (length commits))
        ]
      csvLine =
        T.pack "," `T.intercalate` details
  return csvLine

-- | "Blogg, Joe" becomes "Joe Bloggs".
formatName :: Text -> Text
formatName t =
  if T.isInfixOf "," t
    then T.intercalate " " (map trim (reverse (T.split (== ',') t)))
    else t
  where
    trim :: Text -> Text
    trim = T.pack . dropWhileEnd isSpace . dropWhile isSpace . T.unpack
