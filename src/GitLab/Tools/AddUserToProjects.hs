{-# LANGUAGE OverloadedStrings #-}

module GitLab.Tools.AddUserToProjects where

import qualified Data.Text as T
import GitLab
import GitLab.Tools.Common
import GitLab.Tools.Types

addUserToProjectForksGo :: Options -> String -> IO ()
addUserToProjectForksGo opts tok
  | addUserToProjectForks opts && null (username opts) =
      error "--add-user-to-projects must be accompanied with --user <username>"
  -- add user as a member to all projects with a given name, as Reporter
  | addUserToProjectForks opts && not (null (username opts)) =
      runGitLab
        ( defaultGitLabServer
            { url = T.pack (gitlabUrl opts),
              token = AuthMethodToken (T.pack tok)
            }
        )
        ( do
            filterByUsernames <- filterUsernames (usersFilename opts) (usersGroup opts)
            addUserToProjectForksHelper (T.pack (username opts)) (T.pack (projectOpt opts), T.pack (projectIdOpt opts)) filterByUsernames
        )
  | otherwise =
      error "--add-user-to-projects flag combinations not supported."

addUserToProjectsInGroupGo :: Options -> String -> IO ()
addUserToProjectsInGroupGo opts tok
  | addUserToProjectsInGroup opts && null (username opts) =
      error "--add-user-to-projects-in-group must be accompanied with --user <username>"
  | addUserToProjectsInGroup opts && null (groupOpt opts) && null (groupIdOpt opts) =
      error "--add-user-to-projects-in-group must be accompanied with --group <group> or --group-id <group-id>"
  | addUserToProjectsInGroup opts && not (null (groupOpt opts)) && not (null (groupIdOpt opts)) =
      error "--add-user-to-projects-in-group can only one of --group or --group-id"
  | addUserToProjectsInGroup opts && not (null (groupOpt opts)) = do
      runGitLab
        ( defaultGitLabServer
            { url = T.pack (gitlabUrl opts),
              token = AuthMethodToken (T.pack tok)
            }
        )
        ( do
            filterByUsernames <- filterUsernames (usersFilename opts) (usersGroup opts)
            groupSearch <- searchGroup (T.pack (groupOpt opts))
            case groupSearch of
              [] -> error ("--add-user-to-projects-in-group group " <> groupOpt opts <> " not found")
              [grp] -> do addTheGroup grp filterByUsernames
              (_ : _) -> error ("--add-user-to-projects-in-group too many groups found for " <> groupOpt opts)
        )
  | addUserToProjectsInGroup opts && not (null (groupIdOpt opts)) = do
      runGitLab
        ( defaultGitLabServer
            { url = T.pack (gitlabUrl opts),
              token = AuthMethodToken (T.pack tok)
            }
        )
        ( do
            filterByUsernames <- filterUsernames (usersFilename opts) (usersGroup opts)
            groupSearch <- group (read (groupIdOpt opts))
            case groupSearch of
              Left err -> error ("--add-user-to-projects-in-group group error: " <> show err)
              Right Nothing -> error ("--add-user-to-projects-in-group group with ID " <> groupIdOpt opts <> " not found")
              Right (Just grp) -> addTheGroup grp filterByUsernames
        )
  | otherwise =
      error "--add-user-to-projects-in-group flag combinations not supported."
  where
    addTheGroup grp filterByUsernames = do
      projs <- groupProjects grp defaultGroupProjectFilters
      mapM_
        ( \prj ->
            addUserToProjectForksHelper (T.pack (username opts)) (project_path prj, "") filterByUsernames
        )
        projs
