{-# LANGUAGE OverloadedStrings #-}

module GitLab.Tools.Plagiarism where

import Control.Monad
import Control.Monad.IO.Class
import qualified Data.ByteString.Char8 as C
import Data.ByteString.Lazy.Char8 (unpack)
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import GitLab
import GitLab.Tools.Common
import GitLab.Tools.Types
import Network.HTTP.Client
import Stanford.Moss
import System.Directory
import System.FilePath.Posix
import System.IO.Temp
import System.Random
import Text.Read

plagiarismGo :: Options -> String -> IO ()
plagiarismGo opts tok
  | plagiarism opts
      && ( (mossUserId opts == "")
             || (mossLang opts == "")
             || (plagiarismFilePaths opts == "")
             || (plagiarismProject opts == "")
             || (parentUser opts == "")
             || (usersFilename opts == "" && usersGroup opts == "")
         ) =
      putStrLn
        "--plagiarism needs a MOSS user, MOSS language, a parent user, and either --usersGroup or --usersFilename"
  | plagiarism opts = do
      let files = T.splitOn "," (T.pack (plagiarismFilePaths opts))
      runGitLab
        ( defaultGitLabServer
            { url = T.pack (gitlabUrl opts),
              token = AuthMethodToken (T.pack tok)
            }
        )
        ( do
            usernames <- fromJust <$> filterUsernames (usersFilename opts) (usersGroup opts)
            reports <-
              runMoss
                files
                usernames
                (T.pack (parentUser opts))
                (T.pack (plagiarismProject opts))
                (T.pack (mossUserId opts))
                (T.pack (mossLang opts))
                (T.pack (mossMaxMatch opts))
                (T.pack (branchOpt opts))
            liftIO (putStrLn "MOSS reports:")
            liftIO $ mapM_ T.putStr reports
        )
  | otherwise =
      error "--plagiarism flag combinations not supported."

runMoss :: [Text] -> [Text] -> Text -> Text -> Text -> Text -> Text -> Text -> GitLab [Text]
runMoss filepaths usernames skeletonUser projectName mossUserKey lang maxMatch parentBranch =
  mapM
    ( \fpath -> do
        liftIO $ putStrLn ("Processing file " ++ T.unpack fpath ++ "\n")
        liftIO $
          putStrLn
            ( "getting handed out code "
                ++ T.unpack skeletonUser
                ++ "/"
                ++ T.unpack projectName
                ++ "\n"
            )
        skeletonCode <- do
          let branchName =
                if T.null parentBranch then T.pack "master" else parentBranch
          result <- getCode fpath branchName skeletonUser projectName
          case result of
            Left s -> error s
            Right code -> return code
        -- code <- getCode fpath "master" skeletonUser projectName
        -- return $ fromRight (error "cannot get skeleton code") code
        userSubmissions <-
          mapM
            ( \usr -> do
                liftIO
                  ( putStrLn
                      ( "getting "
                          ++ T.unpack fpath
                          ++ " from "
                          ++ T.unpack usr
                          ++ "/"
                          ++ T.unpack projectName
                      )
                  )
                maybe_userCode <- getCode fpath (if T.null parentBranch then T.pack "master" else parentBranch) usr projectName
                case maybe_userCode of
                  Left _ -> return Nothing
                  Right userCode -> return (Just (usr, userCode))
            )
            usernames
        liftIO (putStrLn "sending job to moss")
        result <- moss fpath skeletonCode (catMaybes userSubmissions) mossUserKey lang maxMatch
        liftIO $ putStrLn ("File " ++ T.unpack fpath ++ " processed.\n")
        return result
    )
    filepaths

moss :: Text -> Text -> [(Text, Text)] -> Text -> Text -> Text -> GitLab Text
moss skeletonFilename skeletonData studentCode mossUserKey lang maxMatch = do
  let language =
        case T.unpack lang of
          "C" -> C
          "CPP" -> CPP
          "Java" -> Java
          "CSharp" -> CSharp
          "Python" -> Python
          "VisualBasic" -> VisualBasic
          "Javascript" -> Javascript
          "FORTRAN" -> FORTRAN
          "ML" -> ML
          "Haskell" -> Haskell
          "Lisp" -> Lisp
          "Scheme" -> Scheme
          "Pascal" -> Pascal
          "Modula2" -> Modula2
          "Ada" -> Ada
          "Perl" -> Perl
          "Matlab" -> Matlab
          "VHDL" -> VHDL
          "Verilog" -> Verilog
          "Spice" -> Spice
          "MIPS" -> MIPS
          "A8086" -> A8086
          "HCL2" -> HCL2
          -- "TCL" -> TCL
          _ -> error "unrecognised MOSS language name"
  let ext =
        case T.unpack lang of
          "C" -> ".c"
          "CPP" -> ".cpp"
          "Java" -> ".java"
          "CSharp" -> ".cs"
          "Python" -> ".py"
          "VisualBasic" -> ".vb"
          "Javascript" -> ".js"
          "FORTRAN" -> ".f"
          "ML" -> ".ml"
          "Haskell" -> ".hs"
          "Lisp" -> ".lisp"
          "Scheme" -> ".scm"
          "Pascal" -> ".pas"
          "Modula2" -> ".mod"
          "Ada" -> ".ada"
          "Perl" -> ".pl"
          "Matlab" -> ".m"
          "VHDL" -> ".vhdl"
          "Verilog" -> ".v"
          "Spice" -> "" -- not sure
          "MIPS" -> "" -- not sure
          "A8086" -> "" -- not sure
          "HCL2" -> "" -- not sure
          -- "TCL" -> ".tcl"
          _ -> error "unrecognised MOSS language name"
  let matches = readMaybe (T.unpack maxMatch) :: Maybe Int
  when (isNothing matches) $ error "Could not parse --mossMaxMatches argument"
  let cfg :: MossCfg
      cfg =
        defaultMossCfg
          { mossLanguage = language,
            mossUser = C.pack (T.unpack mossUserKey),
            mossMaxMatches = (fromJust matches)
          }
  tmpSkelFile <- liftIO $ writeSystemTempFile (takeFileName (T.unpack skeletonFilename)) (T.unpack skeletonData)
  anonIds <- liftIO randomInts
  let anonymisedUserCode =
        zipWith (\(userId, code) genId -> (userId, T.pack (show genId), code)) studentCode anonIds
  userFiles <-
    mapM
      ( \(_, anonId, code) -> do
          userFile <- liftIO $ writeSystemTempFile (T.unpack anonId <> ext) (T.unpack code)
          return (userFile, anonId)
      )
      anonymisedUserCode
  mossResult <- liftIO $
    withMoss cfg $ do
      addBaseFile "Skeleton" tmpSkelFile
      mapM_
        ( \(userFile, anonId) ->
            addFile (T.unpack anonId) userFile
        )
        userFiles
      result <- query (C.pack ("moss-" <> takeFileName (T.unpack skeletonFilename)))
      return $ fromMaybe (error "cannot perform MOSS query") result
  -- remove temporary files
  liftIO $ removeFile tmpSkelFile
  liftIO $ mapM_ (\(fileName, _) -> removeFile fileName) userFiles
  liftIO $ do
    putStrLn
      "revealing the GitLab identities for anonymised users:"
    mapM_
      ( \(realId, anonId, _) ->
          liftIO (putStrLn (T.unpack anonId <> " --> " <> T.unpack realId))
      )
      anonymisedUserCode
  return (T.pack (C.unpack mossResult))

randomInts :: IO [Int]
randomInts =
  randomRs (0, 1000000) <$> getStdGen

getCode :: Text -> Text -> Text -> Text -> GitLab (Either String Text)
getCode filepath theBranch userName projectName = do
  maybe_proj <- projectWithPathAndName userName projectName
  case maybe_proj of
    Left err -> return (Left ("getCode: HTTP error - " <> unpack (responseBody err)))
    -- error ("getCode: exception performing GitLab search project request: " <> T.unpack username <> "/" <> T.unpack projectName)
    Right Nothing -> return (Left "getCode: could not find project")
    -- error ("getCode: project not found: " <> T.unpack username <> "/" <> T.unpack projectName)
    Right (Just proj) -> do
      maybe_file <- repositoryFile proj filepath theBranch
      case maybe_file of
        Left st -> return (Left ("repo file not found: " <> show st))
        -- error ("repo file not found: " <> show st)
        Right Nothing -> return (Left "repo file not found")
        -- error "repo file not found"
        Right (Just repoFile) -> do
          let blobHash = repository_file_blob_id repoFile
          attempt <- repositoryFileBlob (project_id proj) blobHash
          case attempt of
            Left st -> error ("unable to download file blob: " <> show st)
            Right contents ->
              return (Right (T.pack contents))
