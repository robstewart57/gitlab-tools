{-# LANGUAGE OverloadedStrings #-}

module GitLab.Tools.MergeRequest where

import Control.Monad.IO.Class
import Data.Maybe
import qualified Data.Text as T
import qualified Data.Text.IO as T
import GitLab
import GitLab.Tools.Types

mergeRequestGo :: Options -> String -> IO ()
mergeRequestGo opts tok
  | mergeRequestOpt opts =
      if usersFilename opts == ""
        || projectOpt opts == ""
        || mergeRequestSourceNamespace opts == ""
        || mergeRequestTitle opts == ""
        || mergeRequestDescription opts == ""
        then
          error
            "--mergeRequest requires --userFilename and --mergeRequestsSourceUser and --mergeRequestTitle and --mergeRequestDescription and --project"
        else do
          text <- T.readFile (usersFilename opts)
          let usernames = T.splitOn "," text
          runGitLab
            ( defaultGitLabServer
                { url = T.pack (gitlabUrl opts),
                  token = AuthMethodToken (T.pack tok)
                }
            )
            ( do
                sourceProject <-
                  projectWithPathAndName
                    (T.pack $ mergeRequestSourceNamespace opts)
                    (T.pack (projectOpt opts))
                case sourceProject of
                  Left st -> error ("JSON parse error :" <> show st)
                  Right Nothing -> error "source project not found"
                  Right (Just sourceProjectFound) ->
                    mapM_
                      ( \userName -> do
                          prjct <-
                            projectWithPathAndName
                              userName
                              (T.pack (projectOpt opts))
                          case prjct of
                            Left _st -> return ()
                            Right Nothing -> return ()
                            Right (Just targetProject) -> do
                              let sourceBranch =
                                    T.pack $
                                      if mergeRequestSourceBranch opts /= ""
                                        then mergeRequestSourceBranch opts
                                        else "master"
                              let targetBranch =
                                    T.pack $
                                      if mergeRequestTargetBranch opts /= ""
                                        then mergeRequestTargetBranch opts
                                        else "master"
                              res <-
                                createMergeRequest
                                  sourceProjectFound
                                  sourceBranch
                                  targetBranch
                                  (project_id targetProject)
                                  (T.pack $ mergeRequestTitle opts)
                                  (T.pack $ mergeRequestDescription opts)
                              case res of
                                Left err ->
                                  liftIO $
                                    putStrLn
                                      ( "unable to create merge request: "
                                          <> show err
                                      )
                                Right Nothing ->
                                  liftIO $
                                    putStrLn
                                      "merge request not found"
                                Right (Just mr) ->
                                  if fromJust (merge_request_has_conflicts mr)
                                    then return () -- delete it again
                                    else
                                      liftIO $
                                        putStrLn
                                          ( "created merge request for project "
                                              <> T.unpack userName
                                              <> "/"
                                              <> projectOpt opts
                                          )
                      )
                      usernames
            )
  | otherwise =
      error "--mergeRequest flag combinations not supported."
