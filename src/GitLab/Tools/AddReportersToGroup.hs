{-# LANGUAGE OverloadedStrings #-}

module GitLab.Tools.AddReportersToGroup
  ( addReportersToGroupGo,
  )
where

import Control.Monad.IO.Class
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import GitLab
import GitLab.Tools.Types
import Network.HTTP.Client
import Network.HTTP.Types.Status

addReportersToGroupGo :: Options -> String -> IO ()
addReportersToGroupGo opts tok
  | addReportersToGroup opts && usersFilename opts == "" = return ()
  | addReportersToGroup opts && (groupOpt opts == "" && groupIdOpt opts == "") =
      putStrLn "you must specify a group too with --group or --group-id"
  | addReportersToGroup opts && addGroupToProjects opts =
      putStrLn
        "you can only choose one of adding users to group or adding group to projects"
  -- add reporters to a group
  | addReportersToGroup opts && usersFilename opts /= "" = do
      text <- T.readFile (usersFilename opts)
      let usernames = T.splitOn "," text
      runGitLab
        ( defaultGitLabServer
            { url = T.pack (gitlabUrl opts),
              token = AuthMethodToken (T.pack tok)
            }
        )
        (addUsersToGroupDbg (groupOpt opts) (groupIdOpt opts) Reporter usernames)
  | otherwise =
      error "--add-reporters-to-group flag combinations not supported."

addUsersToGroupDbg ::
  String -> String -> AccessLevel -> [Text] -> GitLab ()
addUsersToGroupDbg groupName groupId access usernames = do
  liftIO (putStrLn "Finding users...\n")
  usrs <-
    catMaybes
      <$> mapM
        ( \userName ->
            do
              maybeUser <- searchUser userName
              case maybeUser of
                Nothing -> do
                  liftIO $ putStrLn $ T.unpack userName <> " not found on server."
                  return Nothing
                u -> return u
        )
        usernames
  liftIO (putStrLn ("Finding group " <> groupName <> "...\n"))

  grpResult <- findGroup groupName groupId
  case grpResult of
    Left err -> liftIO (putStrLn err)
    Right grp -> do
      liftIO (putStrLn ("\nAdding users to " <> T.unpack (group_name grp) <> "...\n"))
      mapM_
        ( \theUser -> do
            result <-
              addUserToGroup
                grp
                access
                theUser
            case result of
              Left errorStatus ->
                case statusMessage (responseStatus errorStatus) of
                  "Conflict" ->
                    liftIO $
                      putStrLn
                        ( "User "
                            <> show (user_username theUser)
                            <> " already a member of "
                            <> show (group_name grp)
                        )
                  _ -> liftIO (print errorStatus)
              Right Nothing -> liftIO (print ("could not add user to group " <> group_name grp))
              Right (Just member) ->
                liftIO $
                  putStrLn
                    ( "User "
                        <> T.unpack (fromJust (member_username member))
                        <> " added to group "
                        <> T.unpack (group_name grp)
                    )
        )
        usrs

findGroup :: String -> String -> GitLab (Either String Group)
findGroup groupName groupId = do
  case (groupName, groupId) of
    ("", "") -> return (Left "--group or --group-id must be specified.")
    (theName, "") -> do
      grps <- searchGroup (T.pack theName)
      case grps of
        [] -> return (Left ("could not find group " <> groupName <> " with --group"))
        [grp] -> return (Right grp)
        (_ : _) -> return (Left ("Too many groups found searching for " <> groupName))
    ("", theId) -> do
      searchResult <- group (read theId)
      case searchResult of
        Left err -> return (Left (show err))
        Right Nothing -> return (Left ("could not find group " <> groupId <> " with --group-id"))
        Right (Just grp) -> return (Right grp)
    (_, _) -> return (Left "cannot use --group and --group-id together")
