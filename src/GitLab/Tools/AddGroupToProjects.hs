{-# LANGUAGE OverloadedStrings #-}

module GitLab.Tools.AddGroupToProjects where

import Control.Monad
import Control.Monad.IO.Class
import Data.ByteString.Lazy.Char8 (unpack)
import qualified Data.Text as T
import GitLab
import GitLab.Tools.Common
import GitLab.Tools.Types
import Network.HTTP.Client

addGroupToProjectsGo :: Options -> String -> IO ()
addGroupToProjectsGo opts tok
  | not (addReportersToGroup opts)
      && addGroupToProjects opts
      && ( null (groupOpt opts)
             || null (projectOpt opts)
         ) =
      error
        "for --add-group-to-projects you must specify a group name and a project name"
  -- add group as a member to all projects with a given name, as Reporter
  | not (addReportersToGroup opts) && addGroupToProjects opts =
      runGitLab
        ( defaultGitLabServer
            { url = T.pack (gitlabUrl opts),
              token = AuthMethodToken (T.pack tok)
            }
        )
        ( do
            grps <- searchGroup (T.pack (groupOpt opts))
            void $
              liftIO $
                when (null grps) (error ("group not found: " ++ groupOpt opts))
            void $
              liftIO $
                when
                  (length grps > 1)
                  (error ("multiple groups found for: " ++ groupOpt opts))
            -- should only be 1 group with this head
            let grp = head grps
            prjts <- projectsWithName (T.pack (projectOpt opts))

            filteredProjects <-
              filterProjects
                prjts
                (usersFilename opts)
                (usersGroup opts)

            mapM_
              ( \prj -> do
                  result <-
                    addGroupToProject
                      (group_id grp)
                      (project_id prj)
                      Reporter
                  case result of
                    Left st ->
                      liftIO
                        ( putStrLn
                            ( "unable to share project ID "
                                ++ show (project_id prj)
                                ++ " with group ID "
                                ++ show (group_id grp)
                                ++ ". Reason: "
                                ++ unpack (responseBody st)
                            )
                        )
                    Right _details ->
                      liftIO
                        ( putStrLn
                            ( "Added group ID "
                                ++ show (group_id grp)
                                ++ " to project ID "
                                ++ show (project_id prj)
                                ++ " as a Reporter"
                            )
                        )
              )
              filteredProjects
        )
  | otherwise =
      error "--add-group-to-projects flag combinations not supported."
