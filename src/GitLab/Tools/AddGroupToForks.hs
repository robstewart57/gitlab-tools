{-# LANGUAGE OverloadedStrings #-}

module GitLab.Tools.AddGroupToForks where

import Control.Monad
import Control.Monad.IO.Class
import Data.ByteString.Lazy.Char8 (unpack)
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import GitLab
import GitLab.Tools.Common
import GitLab.Tools.Types
import Network.HTTP.Client

addGroupToForksGo :: Options -> String -> IO ()
addGroupToForksGo opts tok
  | not (addReportersToGroup opts)
      && addGroupToForks opts
      && ( null (groupOpt opts)
             || null (projectIdOpt opts)
         ) =
      error
        "for --add-group-to-forks you must specify a group name and a project ID"
  -- add group as a member to all projects with a given name, as Reporter
  | not (addReportersToGroup opts) && addGroupToForks opts =
      runGitLab
        ( defaultGitLabServer
            { url = T.pack (gitlabUrl opts),
              token = AuthMethodToken (T.pack tok)
            }
        )
        ( do
            grps <- searchGroup (T.pack (groupOpt opts))
            void $
              liftIO $
                when (null grps) (error ("group not found: " ++ groupOpt opts))
            void $
              liftIO $
                when
                  (length grps > 1)
                  (error ("multiple groups found for: " ++ groupOpt opts))
            -- should only be 1 group with this head
            let grp = head grps

            forksTry <- lookupProjectForks (read (projectIdOpt opts))
            case forksTry of
              Left err -> error err
              Right prjForks -> do
                filteredProjects <-
                  filterProjects
                    prjForks
                    (usersFilename opts)
                    (usersGroup opts)

                mapM_
                  ( \prjFork -> do
                      result <-
                        addGroupToProject
                          (group_id grp)
                          (project_id prjFork)
                          Reporter
                      case result of
                        Left st ->
                          liftIO
                            ( putStrLn
                                ( "unable to share project fork ID "
                                    ++ show (project_id prjFork)
                                    ++ " with group ID "
                                    ++ show (group_id grp)
                                    ++ ". Reason: "
                                    ++ unpack (responseBody st)
                                )
                            )
                        Right _details ->
                          liftIO
                            ( putStrLn
                                ( "Added group ID "
                                    ++ show (group_id grp)
                                    ++ " to project fork ID "
                                    ++ show (project_id prjFork)
                                    ++ " as a Reporter"
                                )
                            )
                  )
                  filteredProjects
        )
  | otherwise =
      error "--add-group-to-forks flag combinations not supported."

isProjOwnerInUsers :: [Text] -> Project -> Bool
isProjOwnerInUsers usernames prj =
  case project_owner prj of
    Nothing -> False
    Just ownr ->
      fromJust (owner_username ownr)
        `elem` usernames
