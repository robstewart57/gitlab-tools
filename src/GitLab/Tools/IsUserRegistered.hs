{-# LANGUAGE OverloadedStrings #-}

module GitLab.Tools.IsUserRegistered where

import Control.Monad.IO.Class
import qualified Data.Text as T
import qualified Data.Text.IO as T
import GitLab
import GitLab.Tools.Types

isUserRegisteredGo :: Options -> String -> IO ()
isUserRegisteredGo opts tok
  -- ask if users are registered for a given GitLab server
  | not (addReportersToGroup opts)
      && not (addGroupToProjects opts)
      && isRegistered opts
      && usersFilename opts == "" =
      putStrLn "--registered needs a filename with --usersFilename"
  | not (addReportersToGroup opts)
      && not (addGroupToProjects opts)
      && isRegistered opts = do
      text <- T.readFile (usersFilename opts)
      let usernames = T.splitOn "," text
      runGitLab
        ( defaultGitLabServer
            { url = T.pack (gitlabUrl opts),
              token = AuthMethodToken (T.pack tok)
            }
        )
        ( mapM_
            ( \usrName -> do
                res <- searchUser usrName
                -- empty list of returned users
                if null res
                  then liftIO $ putStrLn (T.unpack usrName ++ ": no")
                  else liftIO $ putStrLn (T.unpack usrName ++ ": yes")
            )
            usernames
        )
  | otherwise =
      error "--registered flag combinations not supported."
