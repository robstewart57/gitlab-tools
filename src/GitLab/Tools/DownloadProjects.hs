{-# LANGUAGE OverloadedStrings #-}

module GitLab.Tools.DownloadProjects where

import Control.Monad
import Control.Monad.IO.Class
import Data.Maybe
import qualified Data.Text as T
import qualified Data.Text.IO as T
import GitLab
import GitLab.Tools.Common
import GitLab.Tools.Types

downloadProjectsGo :: Options -> String -> IO ()
downloadProjectsGo opts tok
  | downloadProjects opts && (projectOpt opts == "" && projectIdOpt opts == "") =
      error "--downloadProjects requires --project or --project-id"
  | downloadProjects opts && (projectOpt opts == "" && projectIdOpt opts /= "") = do
      runGitLab
        ( defaultGitLabServer
            { url = T.pack (gitlabUrl opts),
              token = AuthMethodToken (T.pack tok)
            }
        )
        ( do
            liftIO (putStrLn ("Downloading forks of project with ID " <> projectIdOpt opts))
            forksTry <- lookupProjectForks (read (projectIdOpt opts))
            case forksTry of
              Left err -> error err
              Right prjForks -> do
                filteredProjects <-
                  filterProjects
                    prjForks
                    (usersFilename opts)
                    (usersGroup opts)
                mapM_
                  ( \proj -> do
                      let fname =
                            T.unpack (namespace_path (fromJust (project_namespace proj)))
                              <> "-"
                              <> T.unpack (project_name proj)
                              <> ".zip"
                      liftIO (putStrLn ("downloading: " <> fname))
                      void $
                        fileArchive
                          proj
                          Zip
                          fname
                  )
                  filteredProjects
        )
  | downloadProjects opts
      && usersFilename opts /= "" = do
      text <- T.readFile (usersFilename opts)
      let usernames = T.splitOn "," text
      runGitLab
        ( defaultGitLabServer
            { url = T.pack (gitlabUrl opts),
              token = AuthMethodToken (T.pack tok)
            }
        )
        ( do
            allProjects <- projectsWithName (T.pack (projectOpt opts))
            mapM_
              ( \proj -> do
                  let fname =
                        T.unpack (namespace_path (fromJust (project_namespace proj)))
                          <> "-"
                          <> T.unpack (project_name proj)
                          <> ".zip"
                  when
                    ( let ownr = project_owner proj
                       in case ownr of
                            Nothing -> False
                            Just o -> fromJust (owner_username o) `elem` usernames
                    )
                    $ do
                      liftIO (putStrLn ("downloading: " <> fname))
                      void $
                        fileArchive
                          proj
                          Zip
                          fname
              )
              allProjects
        )
  | downloadProjects opts =
      if projectOpt opts == ""
        then
          error
            "--downloadProjects requires --project"
        else
          runGitLab
            ( defaultGitLabServer
                { url = T.pack (gitlabUrl opts),
                  token = AuthMethodToken (T.pack tok)
                }
            )
            ( do
                allProjects <- projectsWithName (T.pack (projectOpt opts))
                mapM_
                  ( \proj -> do
                      let fname =
                            T.unpack (namespace_path (fromJust (project_namespace proj)))
                              <> "-"
                              <> T.unpack (project_name proj)
                              <> ".zip"
                      liftIO (putStrLn ("downloading: " <> fname))
                      void $
                        fileArchive
                          proj
                          Zip
                          fname
                  )
                  allProjects
            )
  | otherwise =
      error "--downloadProjects flag combinations not supported."
