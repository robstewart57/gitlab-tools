{-# LANGUAGE OverloadedStrings #-}

module GitLab.Tools.DeleteProjects where

import Control.Monad.IO.Class
import qualified Data.Text as T
import GitLab
import GitLab.Tools.Common
import GitLab.Tools.Types

deleteProjectsGo :: Options -> String -> IO ()
deleteProjectsGo opts tok
  | deleteProjects opts && (projectOpt opts == "" && projectIdOpt opts == "") =
      error "--deleteProjects requires --project or --project-id"
  | deleteProjects opts && (projectOpt opts == "" && projectIdOpt opts /= "") = do
      runGitLab
        ( defaultGitLabServer
            { url = T.pack (gitlabUrl opts),
              token = AuthMethodToken (T.pack tok)
            }
        )
        ( do
            liftIO (putStrLn ("Deleting projectID with ID " <> show (projectIdOpt opts)))
            maybeProject <- project (read (projectIdOpt opts))
            case maybeProject of
              Left err ->
                liftIO (putStrLn ("error finding project with ID: " <> projectIdOpt opts <> " reason: " <> show err))
              Right Nothing ->
                liftIO (putStrLn ("no project with ID: " <> projectIdOpt opts))
              Right (Just proj) -> do
                filteredProjects <-
                  filterProjects
                    [proj]
                    (usersFilename opts)
                    (usersGroup opts)
                deleteTheSelectedProjects filteredProjects
        )
  | deleteProjects opts && (projectOpt opts /= "" && projectIdOpt opts == "") = do
      runGitLab
        ( defaultGitLabServer
            { url = T.pack (gitlabUrl opts),
              token = AuthMethodToken (T.pack tok)
            }
        )
        ( do
            foundProjects <- projectsWithName (T.pack (projectOpt opts))
            filteredProjects <-
              filterProjects
                foundProjects
                (usersFilename opts)
                (usersGroup opts)
            deleteTheSelectedProjects filteredProjects
        )
  | otherwise =
      error "--delete-projects flag combinations not supported."
  where
    deleteTheSelectedProjects :: [Project] -> GitLab ()
    deleteTheSelectedProjects projs =
      mapM_
        ( \proj -> do
            result <- deleteProject proj
            case result of
              Left err ->
                liftIO (putStrLn ("could not delete: " <> T.unpack (project_path_with_namespace proj) <> " reason: " <> show err))
              Right Nothing ->
                liftIO (putStrLn ("could not delete: " <> T.unpack (project_path_with_namespace proj)))
              Right (Just ()) ->
                liftIO (putStrLn ("deleted: " <> T.unpack (project_path_with_namespace proj)))
        )
        projs
