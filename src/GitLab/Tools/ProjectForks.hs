{-# LANGUAGE OverloadedStrings #-}

module GitLab.Tools.ProjectForks where

import Control.Monad.IO.Class
import qualified Data.Text as T
import GitLab
import GitLab.Tools.Common
import GitLab.Tools.Types

projectForksGo :: Options -> String -> IO ()
projectForksGo opts tok
  | forks opts && null (projectIdOpt opts) =
      error "--forks must also be used with --project"
  -- --projectForks specified with --usersFilename, so search for
  -- projects (--project) only forked by users in the .csv file.
  | forks opts && (not (null (usersFilename opts)) || not (null (usersGroup opts))) = do
      putStrLn ("finding details for project with ID: " <> show (projectIdOpt opts))
      -- exists <- doesFileExist (usersFilename opts)
      -- unless exists $
      --   error ("file does not exist: " <> usersFilename opts)
      -- usersData <- readFile (usersFilename opts)
      -- let usernames = T.splitOn "," (T.pack usersData)
      runGitLab
        ( defaultGitLabServer
            { url = T.pack (gitlabUrl opts),
              token = AuthMethodToken (T.pack tok)
            }
        )
        ( do
            forksCsv <- do
              forksTry <- lookupProjectForks (read (projectIdOpt opts))
              case forksTry of
                Left err -> error err
                Right prjForks -> do
                  filteredProjects <-
                    filterProjects
                      prjForks
                      (usersFilename opts)
                      (usersGroup opts)
                  mapM studentProjectInfo filteredProjects
            -- ( \fork -> do
            --     if fromJust (owner_username (fromJust (project_owner fork))) `elem` usernames
            --       then Just <$> studentProjectInfo fork
            --       else return Nothing
            -- )
            -- prjForks
            liftIO (putStrLn "\nPrinting CSV...\n")
            mapM_ (liftIO . putStrLn . T.unpack) forksCsv
        )
  -- --forks specified but not --usersFilename,
  -- so instead use --project and just search for projects instead.
  -- \| forks opts && null (usersFilename opts) = do
  --     putStrLn ("finding details for project with ID: " <> projectIdOpt opts)
  --     runGitLab
  --       ( defaultGitLabServer
  --           { url = T.pack (gitlabUrl opts),
  --             token = T.pack tok
  --           }
  --       )
  --       ( do
  --           forksCsv <- do
  --             forksTry <- lookupProjectForks (read (projectIdOpt opts))
  --             case forksTry of
  --               Left err -> error err
  --               Right prjForks -> do
  --                 mapM studentProjectInfo prjForks

  --           liftIO (putStrLn "\nPrinting CSV...\n")
  --           mapM_ (liftIO . putStrLn . T.unpack) forksCsv
  --       )
  | otherwise =
      error "--forks flag combinations not supported."
