{-# LANGUAGE OverloadedStrings #-}

module GitLab.Tools.Common where

import Control.Monad
import Control.Monad.IO.Class
import qualified Data.ByteString.Lazy.Char8 as BSL
import Data.Char (isSpace)
import Data.List (dropWhileEnd)
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import GitLab
import Network.HTTP.Client

-- | helper function to add a user to a project
addUserToProjectForksHelper ::
  -- | username of the user being added as a Reporter
  Text ->
  -- | (name,ID) of the project when looking for forks (use only one), where the ID is the ID of the parent project
  (Text, Text) ->
  -- | optionally a list of usernames, only add reporter to forks by these users
  Maybe [Text] ->
  GitLab ()
addUserToProjectForksHelper userName (projName, projId) userFilter = do
  maybeUser <- searchUser userName
  void $
    liftIO $
      when (isNothing maybeUser) (error ("user not found: " <> T.unpack userName))
  let Just usr = maybeUser
  foundProjects <- do
    case (T.null projName, T.null projId) of
      (True, True) -> error "addUserToProjectForks: requires --project or --project-id"
      (False, False) -> error "addUserToProjectForks: --project and --project-id cannot be used together"
      (False, True) -> projectsWithName projName
      (True, False) -> do
        parentProjLookup <- project (read (T.unpack projId))
        case parentProjLookup of
          Left e -> error ("addUserToProjectForks: error looking for project - " <> show e)
          Right Nothing -> error ("addUserToProjectForks: unable to find project - " <> T.unpack projId)
          Right (Just parentProj) -> do
            forksLookup <- projectForks (project_path_with_namespace parentProj)
            case forksLookup of
              Left e -> error ("addUserToProjectForks: error looking for project forks - " <> show e)
              Right allForks -> return allForks
  mapM_
    ( \prj -> do
        case userFilter of
          Nothing -> doAdd prj usr
          Just onlyTheseForkers -> do
            case project_owner prj of
              Nothing -> return ()
              Just theOwner -> do
                case owner_username theOwner of
                  Nothing -> return ()
                  Just theOwnerUsername -> do
                    when (theOwnerUsername `elem` onlyTheseForkers) $
                      doAdd prj usr
    )
    foundProjects
  where
    doAdd prj usr = do
      result <-
        addMemberToProject
          prj
          Reporter
          usr
      case result of
        Left st ->
          liftIO
            ( putStrLn
                ( "unable to share project "
                    ++ show (project_id prj)
                    ++ " with user "
                    ++ T.unpack (user_username usr)
                    ++ ". Reason: "
                    ++ BSL.unpack (responseBody st)
                )
            )
        Right _details ->
          liftIO
            ( putStrLn
                ( "Added user "
                    ++ T.unpack (user_username usr)
                    ++ " to project "
                    ++ show (project_id prj)
                    ++ " with Reporter role"
                )
            )

filterUsernames :: String -> String -> GitLab (Maybe [Text])
filterUsernames usersFile usersGrp = do
  case (null usersFile, null usersGrp) of
    (True, True) -> return Nothing
    (False, True) -> do
      liftIO (putStrLn ("filtering with " <> usersFile <> " CSV file"))
      usersData <- liftIO (readFile usersFile)
      return $ Just $ T.splitOn "," (T.pack usersData)
    (True, False) -> do
      liftIO (putStrLn ("filtering with " <> usersGrp <> " group membership"))
      grps <- searchGroup (T.pack usersGrp)
      case grps of
        [] -> error "--usersGroup group not found"
        [grp] -> do
          searchMembers <- membersOfGroup grp
          case searchMembers of
            Left _ -> error "unable to find --usersGroup group"
            Right foundMembers -> return (Just $ map (fromJust . member_username) foundMembers)
    (True, True) ->
      error "--usersFilename and --usersGroup cannot both be defined"

lookupProjectForks :: Int -> GitLab (Either String [Project])
lookupProjectForks prjId = do
  projLookup <- project prjId
  case projLookup of
    Left err -> return (Left ("Error searching for project " <> show prjId <> ", " <> show err))
    Right Nothing -> return (Left ("project " <> show prjId <> " not found"))
    Right (Just prj) -> do
      forksLookup <- projectForks (project_path_with_namespace prj)
      case forksLookup of
        Left err -> return (Left ("Error searching for forks for project " <> show prjId <> ", " <> show err))
        Right prjForks -> return (Right prjForks)

studentProjectInfo :: Project -> GitLab Text
studentProjectInfo proj = do
  isPassing <- projectCISuccess proj
  commits <- repoCommits proj
  projMembers <- membersOfProject proj
  -- let headers :: [String]
  --     headers =
  --       [ "URL",
  --         "CI",
  --         "members",
  --         "visibility",
  --         "num_commits",
  --         "last_commit_hash"
  --       ]
  let details =
        [ case project_owner proj of
            Nothing -> ""
            Just own -> formatName (owner_name own),
          T.intercalate ";" (map (fromJust . member_username) projMembers),
          project_web_url proj,
          if isPassing then "passing" else "failing",
          case length commits of
            0 -> ""
            _ -> fromJust (commit_committed_date (head commits)),
          case length commits of
            0 -> ""
            _ -> commit_id (head commits),
          fromMaybe "" (project_visibility proj),
          T.pack (show (length commits))
        ]
      csvLine =
        T.pack "," `T.intercalate` details
  return csvLine

-- | "Blogg, Joe" becomes "Joe Bloggs".
formatName :: Text -> Text
formatName t =
  if T.isInfixOf "," t
    then T.intercalate " " (map trim (reverse (T.split (== ',') t)))
    else t
  where
    trim :: Text -> Text
    trim = T.pack . dropWhileEnd isSpace . dropWhile isSpace . T.unpack

-- | users may use --usersFilename or --usersGroup. The user may
-- decide to use neither, but they cannot use both.
filterProjects ::
  -- | the list of projects to filter
  [Project] ->
  -- | the CSV filename with usernames of members to filter for
  String ->
  -- | the GitLab group name with members to filter for
  String ->
  GitLab [Project]
filterProjects prjts "" "" = return prjts
filterProjects prjts usersInFile usersInGroup = do
  usernameList <- fromJust <$> filterUsernames usersInFile usersInGroup
  return (filter (isProjOwnerInUsers usernameList) prjts)

isProjOwnerInUsers :: [Text] -> Project -> Bool
isProjOwnerInUsers usernames prj =
  case project_owner prj of
    Nothing -> False
    Just ownr ->
      fromJust (owner_username ownr)
        `elem` usernames
